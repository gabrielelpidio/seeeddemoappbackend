require("dotenv").config();

const config = {
  port: process.env.PORT || 3000,
  dbURL: process.env.DB_CONNECT || " ",
};

module.exports = config;
