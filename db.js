const db = require("mongoose");

db.Promise = global.Promise;

/**
 * Connects to the database
 * @param  {String} url
 */
async function connect(url) {
  await db.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
}

module.exports = connect;
