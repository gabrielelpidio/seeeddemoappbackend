# Testimonials App Backend

A simple API of Testimonials for Seeed technical test

> Made using Node.js, Express and MongoDB

## How to run:

---

#### Requirements to run:

- **docker**

- **docker-compose**

#### Development:

```bash
docker-compose -f docker-compose-dev.yml up
```

#### Production:

```bash
docker-compose up
```

## Endpoints:

---

### List Testimonials

  Returns an array with all the testimonials.

* **URL**
  /testimonials

* **Method:**
  `GET`

* **URL Params**
  none

* **Data Params**
  none

* **Success Response:**
  
  * **Code:** 200 
    
    **Content:** `[{"name": "Bob", "content": "very good"}, {"name": "Lia", "content": "Amazing"}]`

* **Error Response:**
  
  * **Code:** 500 INTERNAL SERVER ERROR  
    
    **Content:** `{error: 'Internal server error'}`

* **Sample Call:**
  
  ```javascript
    fetch('/testimonials')
  ```



### Add Testimonial

Adds a testimonial

- **URL** /testimonials

- **Method:** `POST`

- **URL Params** none

- **Data Params** 
  
  **Required:**
  
  `name=[string]`
  
  `content=[string]`

- **Success Response:**
  
  - **Code:** 200 
    
    **Content:** `{name: "Bob", content: "very good", "date": "2021-02-28T03:46:14.910Z"}`

- **Error Response:**
  
  - **Code:** 400 INVALID INFO 
    
    **Content:** `{error: 'Invalid Info'}`

- **Sample Call:**
  
  ```javascript
    fetch('/testimonials', { 
    method: "POST", 
    body: JSON.strigify({
            name: 'Lia'
            content: 'Amazing'
        })
    })
  ```

### 

### Delete Testimonial

Deletes a Testimonial

- **URL** /testimonials/:id

- **Method:** `DELETE`

- **URL Params** 
  
  **Required:**
  
  `id=[string]` 

- **Data Params** none

- **Success Response:**
  
  - **Code:** 200 
    
    **Content:** `{Deleted testimonial: ':id'}`

- **Error Response:**
  
  - **Code:** 500 INTERNAL SERVER ERROR 
    
    **Content:** `{error: 'Internal server error'}`

- **Sample Call:**
  
  ```javascript
    fetch('/testimonials/:id', { method: "DELETE", })
  ```
