const store = require("./store");

/**
 *
 * @typedef {Object} userTestimonial a Testimonial that consists of a name and content
 * @property {string} name name of the owner of the testimonial
 * @property {string} content content of the testimonial
 *
 * @typedef {Object} Testimonial
 * @property {string} name name of the owner of the testimonial
 * @property {string} content content of the testimonial
 * @property {Date} date exact date when the testimonial was added
 */

/**
 *
 * @param  {userTestimonial} testimonial
 * @returns {Promise<fullTestimonial|Error>} On fulfillment returns the complete
 * testimonial, on reject returns an Error
 *
 */
function addTestimonial(testimonial) {
  return new Promise((resolve, reject) => {
    if (!testimonial) {
      console.error("[messageController]: There is no testimonial");
      reject("No valid data");
      return;
    }

    const fullTestimonial = {
      ...testimonial,
      date: new Date(),
    };
    store
      .add(fullTestimonial)
      .then(() => resolve(fullTestimonial))
      .catch((e) => reject(e));
  });
}

/**
 * Retrieves all the testimonials
 * @returns {Promise<Array<Testimonial>>} On fulfillment returns the an array of
 * all the Testimonials, on reject returns an Error
 */
function listTestimonial() {
  return new Promise((resolve, reject) => {
    store
      .list()
      .then((data) => {
        resolve(data);
      })
      .catch((e) => {
        reject(e);
      });
  });
}
/**
 * Deletes a testimonial
 * @param  {String} id
 */
function deleteTestimonial(id) {
  return new Promise((resolve, reject) => {
    if (!id) {
      reject("Invalid id");
      return;
    }
    store
      .remove(id)
      .then(() => {
        resolve();
      })
      .catch((e) => {
        reject(e);
      });
  });
}

module.exports = {
  addTestimonial,
  listTestimonial,
  deleteTestimonial,
};
