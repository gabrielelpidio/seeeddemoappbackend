const Model = require("./model");

/**
 *
 * @typedef {Object} Testimonial
 * @property {string} name name of the owner of the testimonial
 * @property {string} content content of the testimonial
 * @property {Date} date exact date when the testimonial was added
 */

/**
 * @param {Testimonial} testimonial
 */
function addTestimonial(testimonial) {
  const myTestimonial = new Model(testimonial);
  return myTestimonial.save();
}

/**
 * Retrieves all testimonials in the database
 * @returns {Array<Testimonial>} testimonials
 */
async function listTestimonials() {
  const testimonials = await Model.find();
  return testimonials;
}

/**
 * Removes one testimonial from the database
 * @param  {String} id id of the user
 */
function removeTestimonial(id) {
  return Model.deleteOne({
    _id: id,
  });
}

module.exports = {
  add: addTestimonial,
  list: listTestimonials,
  remove: removeTestimonial,
};
