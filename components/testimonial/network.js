const express = require("express");

const response = require("../../network/response");

const controller = require("./controller");

const router = express.Router();

router.get("/", (req, res) => {
  controller
    .listTestimonial()
    .then((testimonialList) => {
      response.success(req, res, testimonialList, 200);
    })
    .catch((e) => {
      response.error(req, res, "Internal Server Error", 500, e);
    });
});

router.post("/", (req, res) => {
  controller
    .addTestimonial(req.body.testimonial)
    .then((testimonial) => {
      response.success(req, res, testimonial, 201);
    })
    .catch((e) => {
      response.error(req, res, "Invalid info", 400, e);
    });
});

router.delete("/:id", function (req, res) {
  controller
    .deleteTestimonial(req.params.id)
    .then(() => {
      response.success(req, res, `Deleted testimonial: ${req.params.id}`, 200);
    })
    .catch((e) => {
      response.error(req, res, "Internal Server Error", 500, e);
    });
});

module.exports = router;
