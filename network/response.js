/**
 * Predefined success response
 * @param  {req} req
 * @param  {res} res
 * @param  {String} body body of the response
 * @param  {Number} status status code
 */
exports.success = function (req, res, body, status) {
  res.status(status || 200).send({
    error: "",
    body: body,
  });
};

/**
 * Predefined error response
 * @param {req} req
 * @param {res} res
 * @param {String} message
 * @param {Number} status
 * @param {Error} details
 */
exports.error = function (req, res, message, status, details) {
  console.error("[response error] " + details);

  res.status(status || 500).send({
    error: message,
    body: "",
  });
};
