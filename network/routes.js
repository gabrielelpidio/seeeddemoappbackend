const express = require("express");
const testimonial = require("../components/testimonial/network");

const routes = function (server) {
  server.use("/testimonial", testimonial);
};

module.exports = routes;
