/** Entry point of the app*/

const express = require("express");
const bodyParser = require("body-parser");

const config = require("./config");

const router = require("./network/routes");
const cors = require("cors");

const db = require("./db");

const app = express();
app.use(cors());

/**
 * Connects to the database
 */
db(config.dbURL);

app.use(bodyParser.json());

/**
 * uses the router from network, removing the need to
 * implement each route on this file
 */
router(app);

/**
 * Exposes server on configured port
 */
app.listen(config.port, () =>
  console.log(`Example app is listening on port ${config.port}`)
);
